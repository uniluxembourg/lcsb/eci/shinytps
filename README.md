# Welcome to ShinyTPs

ShinyTPs is an  R package for curating transfromation products from the PubChem Metabolism and Metabolites section. The app  utilizes the already text mined chemical names in PubChem to create potential transformation reactions that the user then validates and saves in a format suitable for integration into the PubChem transformations section or for use in further workflows. For a more in depth description please refer to the documentation file.

## Quick Start
To download ShinyTPs use the following code:
```{r install, eval=FALSE}
devtools::install_git("https://gitlab.lcsb.uni.lu/eci/shinytps.git")

```

If you recieve an error message due to the chemdoodle package please check if you have a 64-bit version of JAVA installed and set your JAVA_HOME in R before running the installaion. One option for installing 64-bit JAVA is here: https://www.oracle.com/java/technologies/downloads/#jdk21-windows pick the "Installer".
Then add the following to the top of your script: 
```{r chunk-label, eval=FALSE}
Sys.setenv(JAVA_HOME="C:/Program Files/Java/jdk-21") 
```
The app can then be launched by first reading in the file containing the list of compounds for which you want to curate information (see the ShinyTPs_test_file.csv for examples). Then generating the tables used inside the app, and finally launching the app with the Run_ShinyTPs function. Please replace the directory and column names to those applicable to your file.

```{r chunk-label, eval=FALSE}
Cmpd <- read_shiny_data(directory = "C:/Users/testing/ShinyTPs_test_file.csv", 
                        SMILES_col = "isosmiles", 
                        name_col = "cmpdname", 
                        CID_col = "cid")

Table_list <- generate_ShinyTP_tables(cmpd = Cmpd)

Run_ShinyTPs(HSDB_text_table = Table_list[[1]],
             HSDB_table = Table_list[[2]],
             PubChem_table = Table_list[[3]],
             PubChem_table_TPs = Table_list[[4]],
             cmpd_info = Table_list[[5]],
             Reaction_table = Table_list[[6]],
             UIC_table = Table_list[[7]])
```
## License

Content in this repository is shared under these license conditions:

Code: Artistic-2.0
Data: CC-BY 4.0 (Creative Commons Attribution 4.0 International)

Copyright (c) 2017-2023 University of Luxembourg
